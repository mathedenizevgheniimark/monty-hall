const rl = require("readline-sync");

const zufallTuer = (anzahlTueren) =>
  Math.floor(Math.random() * (anzahlTueren + 1));

const montyHallExperiment = ({
  anzahlTueren,
  weggenommen,
  wechseln,
  logging,
}) => {
  let gewaehlt = zufallTuer(anzahlTueren);
  let gewinner = zufallTuer(100);

  if (logging) {
    console.log(
      `Es gibt ${anzahlTueren} Türen. \n Sie wählen Tür ${zufallTuer(
        anzahlTueren
      )}.\n Die Gewinnwahrscheinlichkeit für diese Tür liegt bei ${
        1 / anzahlTueren
      }.\n Die Wahrscheinlichkeit, dass sich das Auto in einer der ${
        anzahlTueren - 1
      } weiteren Türen befindet, liegt bei ${
        (anzahlTueren - 1) / anzahlTueren
      }.\n
          Nun werden ${weggenommen} dieser ${
        anzahlTueren - 1
      } Türen geöffnet und stellen sich als Nieten heraus.\n
          Die Wahrscheinlichkeit von ${
            (anzahlTueren - 1) / anzahlTueren
          } konzentriert sich auf ${
        anzahlTueren - weggenommen === 2
          ? "eine Tür"
          : anzahlTueren - 1 - weggenommen + " Türen"
      }.
          Die Gewinnwahrscheinlichkeit der übrigen (nicht gewählten) Türen liegt bei jeweils ${
            (anzahlTueren - 1) / anzahlTueren / (anzahlTueren - 1 - weggenommen)
          }`
    );
  }

  const evaluation = (wechseln) => {
    const gewaehltGewonnen = gewaehlt === gewinner;

    const richtig =
      (!wechseln && gewaehltGewonnen) || (wechseln && !gewaehltGewonnen);
    if (logging) {
      console.log(
        `Sie haben die Tür ${!wechseln ? "nicht" : ""} gewechselt und ${
          richtig ? "ein Auto gewonnen!!!" : "eine Niete gezogen :("
        }\n\n`
      );
    }

    return richtig;
  };
  if (wechseln !== undefined) {
    return evaluation(wechseln);
  } else {
    const answer = rl.question("Wollen Sie die Tür wechseln? (y/n)");
    return evaluation(answer === "y");
  }
};

const test = ({ amount, anzahlTueren, weggenommen, wechseln }) => {
  let richtigZaehler = 0;
  let falschZaehler = 0;
  for (let i = 0; i < amount; i++) {
    const result = montyHallExperiment({
      anzahlTueren,
      weggenommen,
      logging: false,
      wechseln,
    });
    if (result === true) richtigZaehler++;
    else falschZaehler++;
  }
  console.log(`${richtigZaehler} mal richtig und ${falschZaehler} mal falsch`);
};

const run = async () => {
  console.log("Originalvariante:");
  montyHallExperiment({
    anzahlTueren: 3,
    weggenommen: 1,
    logging: true,
  });

  console.log("Variante A:");
  montyHallExperiment({
    anzahlTueren: 100,
    weggenommen: 98,
    logging: true,
  });

  console.log("Variante B:");
  montyHallExperiment({
    anzahlTueren: 100,
    weggenommen: 1,
    logging: true,
  });
  console.log("Führe Originalvariante 10000 mal mit wechseln aus:");
  test({ amount: 10000, anzahlTueren: 3, weggenommen: 1, wechseln: true });
  console.log("Führe Originalvariante 10000 mal ohne wechseln aus:");
  test({ amount: 10000, anzahlTueren: 3, weggenommen: 1, wechseln: false });

  console.log("Führe Variante A 10000 mal mit wechseln aus:");
  test({ amount: 10000, anzahlTueren: 100, weggenommen: 98, wechseln: true });
  console.log("Führe Variante A 10000 mal ohne wechseln aus:");
  test({ amount: 10000, anzahlTueren: 100, weggenommen: 98, wechseln: false });

  console.log("Führe Variante B 10000 mal mit wechseln aus:");
  test({ amount: 10000, anzahlTueren: 100, weggenommen: 1, wechseln: true });
  console.log("Führe Variante B 10000 mal ohne wechseln aus:");
  test({ amount: 10000, anzahlTueren: 100, weggenommen: 1, wechseln: false });

  console.log(
    "Wie man sieht, erweisen sich die theoretischen Berechnungen als richtig"
  );
};

run();
